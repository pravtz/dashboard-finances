import React from 'react';

import { Container, TitleContainer, Controllers } from './styled';

interface IContentHeaderProps {
  title: string;
  children?: React.ReactNode;
  lineColor: string;
}
/**
 * Select Input
 * @param lineColor string
 * @param title string
 * @param children ReactNode - opcional (possible to use the selectInput component)
 */
const ContentHeader: React.FC<IContentHeaderProps> = ({
  lineColor,
  title,
  children,
}) => (
  <Container>
    <TitleContainer lineColor={lineColor}>
      <h1>{title}</h1>
    </TitleContainer>
    <Controllers>{children}</Controllers>
  </Container>
);

export default ContentHeader;
