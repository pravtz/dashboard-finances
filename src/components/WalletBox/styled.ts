import styled from 'styled-components';

interface IContainerProps {
  color: string;
}

export const Container = styled.div<IContainerProps>`
  width: 32%;
  height: 150px;

  margin: 10px 0;

  background-color: ${(props) => props.color};
  color: ${(props) => props.theme.colors.white};

  box-sizing: border-box;
  border-radius: 7px;
  padding: 10px 20px;

  position: relative;
  overflow: hidden;
  > span {
    font-size: 18px;
    font-weight: 700;
  }

  > img {
    position: absolute;
    height: 110%;
    top: -10px;
    right: -30px;
    opacity: 0.3;
  }
`;
