import React, { useMemo } from 'react';

import CountUp from 'react-countup';
import { Container } from './styled';
import dolarImage from '../../assets/dollar.svg';
import arrowUpImage from '../../assets/arrow-up.svg';
import arrowDownImage from '../../assets/arrow-down.svg';

interface IWalletBoxProps {
  title: string;
  amount: number;
  footerlabel: string;
  icon: 'dolar' | 'arrowUp' | 'arrowDown' | undefined;
  color: string;
}

const WalletBox: React.FC<IWalletBoxProps> = ({
  title,
  amount,
  footerlabel,
  icon,
  color,
}) => {
  const iconSelected = useMemo(() => {
    if (icon === 'dolar') return dolarImage;
    if (icon === 'arrowUp') return arrowUpImage;
    if (icon === 'arrowDown') return arrowDownImage;
    return dolarImage;
  }, [icon]);

  return (
    <Container color={color}>
      <span>{title}</span>
      <h2>
        <CountUp
          end={amount}
          prefix={'R$ '}
          separator={'.'}
          decimal={','}
          decimals={2}
        />
      </h2>

      <small>{footerlabel}</small>
      <img src={iconSelected} alt={title} />
    </Container>
  );
};

export default WalletBox;
