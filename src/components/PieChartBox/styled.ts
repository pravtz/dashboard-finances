import styled from 'styled-components';

interface ILegendProps {
  color: string;
}

export const Container = styled.div`
  width: 47%;
  height: 260px;

  margin: 10px 0;

  background-color: ${(props) => props.theme.colors.tertiary};
  color: ${(props) => props.theme.colors.white};

  border-radius: 7px;

  display: flex;
`;
export const SideLeft = styled.div`
  padding: 30px 20px;

  > h2 {
    margin-bottom: 20px;
  }
`;
export const LegendContainer = styled.ul`
  list-style: none;
`;
export const LegendWapper = styled.li<ILegendProps>`
  display: flex;
  align-items: center;
  margin-bottom: 7px;
  font-size: 14px;

  > div {
    background-color: ${(props) => props.color};
    font-size: 14px;
    line-height: 40px;
    width: 50px;
    height: 40px;
    border-radius: 5px;
    text-align-last: center;
  }

  > span {
    margin-left: 5px;
  }
`;
export const SideRight = styled.main`
  display: flex;
  flex: 1;
  justify-content: center;
`;
