import React from 'react';

import Aside from '../Aside';
import Content from '../Content';
import MainHeader from '../MainHeader';

import { Container } from './styled';

const Layout: React.FC = ({ children }) => (
  <Container>
    <MainHeader />
    <Content>{children}</Content>
    <Aside />
  </Container>
);

export default Layout;
