import styled from 'styled-components';

interface ILegendProps {
  color: string;
}

export const Container = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;

  background-color: ${(props) => props.theme.colors.tertiary};
  color: ${(props) => props.theme.colors.white};

  margin: 10px 0;
  padding: 30px 20px;

  border-radius: 7px;
`;
export const ChartConteiner = styled.div`
  height: 340px;
`;
export const Header = styled.header`
  display: flex;
  justify-content: space-between;
  padding: 0 20px;

  > h2 {
    margin-bottom: 20px;

    // padding-left: 16px;
  }
`;
export const LegendConteiner = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: column;
`;
export const Legend = styled.li<ILegendProps>`
  display: flex;
  align-items: center;
  margin-bottom: 7px;

  > div {
    background-color: ${(props) => props.color};
    width: 40px;
    height: 40px;
    border-radius: 5px;
    margin-right: 7px;
  }
`;
