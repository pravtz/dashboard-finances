import React, { useMemo, useState } from 'react';
import { Container, Profile, UserName, Welcome } from './styled';

import { useTheme } from '../../hooks/theme';

import emojis from '../../utils/emojies';
import Toggle from '../Toggle';

const MainHeader: React.FC = () => {
  const { toggleTheme, theme } = useTheme();

  const [darkTheme, setDarkTheme] = useState(
    // eslint-disable-next-line no-unneeded-ternary
    () => (theme.title === 'dark' ? true : false)
  );

  const handleChargeTheme = () => {
    setDarkTheme(!darkTheme);
    toggleTheme();
  };

  const emoji = useMemo(() => {
    const indice = Math.floor(Math.random() * emojis.length);
    return emojis[indice];
  }, []);

  return (
    <Container>
      <Toggle
        labelLeft="Light"
        labelRight="Dark"
        checked={darkTheme}
        onChange={handleChargeTheme}
      />
      <Profile>
        <Welcome>Ola, {emoji}</Welcome>
        <UserName>Ederson Pravtz</UserName>
      </Profile>
    </Container>
  );
};

export default MainHeader;
