import React from 'react';

import { Container, Tag, WrapperContentCard } from './styled';

interface IHistoryFinanceCardProps {
  cardColor: string;
  tagColor: string;
  title: string;
  subtitle: string;
  amount: string;
}

const HistoryFinanceCard: React.FC<IHistoryFinanceCardProps> = ({
  cardColor,
  tagColor,
  title,
  subtitle,
  amount,
}) => (
  <Container color={cardColor}>
    <Tag color={tagColor} />
    <WrapperContentCard>
      <div>
        <span>{title}</span>
        <small>{subtitle}</small>
      </div>
      <h3>{amount}</h3>
    </WrapperContentCard>
  </Container>
);

export default HistoryFinanceCard;
