import styled from 'styled-components';

interface IContainerProps {
  color: string;
}
interface ITagProps {
  color: string;
}

export const Container = styled.div<IContainerProps>`
  background-color: ${(props) => props.color};
  border-radius: 5px;
  margin: 10px 0;
  padding: 12px 10px;

  display: flex;
  align-items: center;

  cursor: pointer;
  transition: all 0.3s;

  &:hover {
    opacity: 0.7;
    transform: translateX(10px);
  }
`;
export const Tag = styled.div<ITagProps>`
  width: 10px;
  height: 50px;

  background-color: ${(props) => props.color};
`;

export const WrapperContentCard = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
  width: 100%;
  > div {
    display: flex;
    flex-direction: column;
    margin-left: 10px;
  }
`;
