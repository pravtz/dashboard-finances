import styled from 'styled-components';

export const Container = styled.div`
  grid-area: AS;
  background-color: ${(props) => props.theme.colors.secondary};
  border-right: 0.4px solid ${(props) => props.theme.colors.gray};
  padding-left: 20px;
`;
export const Header = styled.header`
  display: flex;
`;
export const Title = styled.h1`
  color: ${(props) => props.theme.colors.white};
  font-size: 1.8em;
  line-height: 25px;
  margin-left: 10px;
`;
export const LogImage = styled.img``;
export const MenuContainer = styled.nav`
  margin-top: 50px;
`;
export const MenuItemLink = styled.a`
  display: flex;
  align-items: center;
  text-decoration: none;
  color: ${(props) => props.theme.colors.info};
  margin: 7px 0;

  transition: opacity 0.3s;

  &:hover {
    opacity: 0.7;

    > svg {
      color: ${(props) => props.theme.colors.white};
    }
  }
  > svg {
    color: ${(props) => props.theme.colors.info};
    font-size: 18px;
    margin-right: 8px;
  }
`;
