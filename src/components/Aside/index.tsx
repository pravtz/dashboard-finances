import React from 'react';
import {
  MdDashboard,
  MdArrowDownward,
  MdArrowUpward,
  MdExitToApp,
} from 'react-icons/md';
import logoImg from '../../assets/logo.svg';
import {
  Container,
  Header,
  Title,
  LogImage,
  MenuContainer,
  MenuItemLink,
} from './styled';

const Aside: React.FC = () => (
  <Container>
    <Header>
      <LogImage src={logoImg} alt={'logo'}></LogImage>
      <Title>Minha Carteira</Title>
    </Header>
    <MenuContainer>
      <MenuItemLink href="/dashboard">
        <MdDashboard />
        Dashboard
      </MenuItemLink>
    </MenuContainer>
    <MenuContainer>
      <MenuItemLink href="/list/entry-balance">
        <MdArrowDownward />
        Entradas
      </MenuItemLink>
    </MenuContainer>
    <MenuContainer>
      <MenuItemLink href="/list/exit-balance">
        <MdArrowUpward />
        Saídas
      </MenuItemLink>
    </MenuContainer>
    <MenuContainer>
      <MenuItemLink href="#">
        <MdExitToApp />
        sair
      </MenuItemLink>
    </MenuContainer>
  </Container>
);

export default Aside;
