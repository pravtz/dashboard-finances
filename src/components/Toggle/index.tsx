import { Container, ToggleLabel, ToggleSwitch } from './styled';

interface IToggleProps {
  labelLeft: string;
  labelRight: string;
  checked: boolean;
  onChange: () => void;
}

const Toggle: React.FC<IToggleProps> = ({
  labelLeft,
  labelRight,
  checked,
  onChange,
}) => (
  <Container>
    <ToggleLabel>{labelLeft}</ToggleLabel>
    <ToggleSwitch
      onChange={onChange}
      checked={checked}
      uncheckedIcon={false}
      checkedIcon={false}
    />
    <ToggleLabel>{labelRight}</ToggleLabel>
  </Container>
);

export default Toggle;
