import React from 'react';
import { ThemeProvider } from 'styled-components';
import Routes from './routes';
import { useTheme } from './hooks/theme';
import GlobalStyled from './styles/Global';

const App: React.FC = () => {
  const { theme } = useTheme();
  return (
    <ThemeProvider theme={theme}>
      <Routes />
      <GlobalStyled />
    </ThemeProvider>
  );
};

export default App;
