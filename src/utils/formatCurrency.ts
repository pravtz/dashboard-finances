/**
 * formats an entry number for currency of brazil in string type
 * @example
 *  formatCurrency(2200); // R$2.200,00
 * @param number required
 * @returns string
 * */
const formatCurrency = (current: number): string =>
  current.toLocaleString('pt-br', {
    style: 'currency',
    currency: 'BRL',
  });

export default formatCurrency;
