/**
 * formats the date of the year-month-day pattern to day/month/year
 * @example
 *  formatDate(2020-06-23); // 23/06/2020
 * @param date string required
 * @returns string
 * */
const formatDate = (date: string): string => {
  /** converts days and months into two digits to numbers less than 10 */
  const twodigits = (valor: number) => {
    if (valor <= 9) {
      return `0${valor}`;
    }
    return valor;
  };

  const dataFormated = new Date(date);

  const day = twodigits(dataFormated.getDate());
  const mount = twodigits(dataFormated.getMonth() + 1);
  const year = dataFormated.getFullYear();

  return `${day}/${mount}/${year}`;
};

export default formatDate;
