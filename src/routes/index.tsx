import React from 'react';
import { BrowserRouter } from 'react-router-dom';

import AppRoute from './app.route';

const Routes: React.FC = () => (
  <BrowserRouter>
    <AppRoute />
  </BrowserRouter>
);

export default Routes;
