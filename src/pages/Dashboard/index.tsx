import React, { useState, useMemo } from 'react';
import ContentHeader from '../../components/ContentHeader';
import SelectInput from '../../components/SelectInput';
import WalletBox from '../../components/WalletBox';
import MensageBox from '../../components/MensageBox';
import PieChartBox from '../../components/PieChartBox';
import HistoryBox from '../../components/HistoryBox';
import { Container, Content } from './styled';
import gains from '../../utils/sampleData/gains';
import expenses from '../../utils/sampleData/expenses';
import happy from '../../assets/happy.svg';
import sad from '../../assets/sad.svg';

const Dashboard: React.FC = () => {
  const [monthSelected, setMonthSelected] = useState<number>(
    new Date().getMonth() + 1
  );
  const [yearSelected, setYearSelected] = useState<number>(
    new Date().getFullYear()
  );

  const mouths = [
    { value: 1, label: 'Janeiro' },
    { value: 2, label: 'Fevereiro' },
    { value: 3, label: 'Março' },
    { value: 4, label: 'Abril' },
    { value: 5, label: 'Maio' },
    { value: 6, label: 'Junho' },
    { value: 7, label: 'Julho' },
    { value: 8, label: 'Agosto' },
    { value: 9, label: 'Setembro' },
    { value: 10, label: 'Outubro' },
    { value: 11, label: 'Novembro' },
    { value: 12, label: 'Dezembro' },
  ];

  const years = useMemo(() => {
    const listData = [...gains, ...expenses];
    const uniqueYears: number[] = [];
    const currentYear = new Date().getFullYear();
    uniqueYears.push(currentYear);

    listData.forEach((item) => {
      const date = new Date(item.date);
      const year = date.getFullYear();

      if (!uniqueYears.includes(year)) {
        uniqueYears.push(year);
      }
    });

    return uniqueYears.map((year) => ({
      value: year,
      label: year,
    }));
  }, []);

  const totalExpansive = useMemo(() => {
    let total = 0;

    expenses.forEach((item) => {
      const date = new Date(item.date);
      const month = date.getMonth() + 1;
      const year = date.getFullYear();

      if (month === monthSelected && year === yearSelected) {
        try {
          total += Number(item.amount);
        } catch {
          throw new Error('Invalid amount!');
        }
      }
    });

    return total;
  }, [monthSelected, yearSelected]);

  const totalGains = useMemo(() => {
    let total = 0;

    gains.forEach((item) => {
      const date = new Date(item.date);
      const month = date.getMonth() + 1;
      const year = date.getFullYear();

      if (month === monthSelected && year === yearSelected) {
        try {
          total += Number(item.amount);
        } catch {
          throw new Error('Invalid amount!');
        }
      }
    });

    return total;
  }, [monthSelected, yearSelected]);

  const totalBalance = useMemo(() => {
    const total = totalGains - totalExpansive;
    return total;
  }, [totalExpansive, totalGains]);

  const mensageBalance = useMemo(() => {
    if (totalBalance < 0) {
      return {
        title: 'Ops! Deu ruim!',
        description: 'Sua carteira está negativa!',
        footerText: 'Considere rever seus gastos!',
        icon: sad,
      };
    }
    if (totalBalance === 0) {
      return {
        title: 'Tenha cuidado!',
        description: 'Sua carteira vazia!',
        footerText: 'Considere rever seus gastos! Ta no limite!',
        icon: happy,
      };
    }

    return {
      title: 'Muito Bem!',
      description: 'Sua carteira está positiva!',
      footerText: 'Continue assim. considere investir o seu saldo!',
      icon: happy,
    };
  }, [totalBalance]);

  const realationExpensesVersusGains = useMemo(() => {
    const total = totalExpansive + totalGains;
    const gainsPercent = (totalGains / total) * 100;
    const expensesPercent = (totalExpansive / total) * 100;

    const data = [
      {
        name: 'Entradas',
        value: totalGains,
        percent: Number(gainsPercent.toFixed(1)),
        color: '#e44c4e',
      },
      {
        name: 'Saídas',
        value: totalExpansive,
        percent: Number(expensesPercent.toFixed(1)),
        color: '#F7931B',
      },
    ];

    return data;
  }, [totalExpansive, totalGains]);

  const historyData = useMemo(
    () =>
      mouths
        .map((_, month) => {
          let amouthEntry = 0;
          gains.forEach((gain) => {
            const date = new Date(gain.date);
            const gainMonth = date.getMonth();
            const gainYear = date.getFullYear();

            if (gainMonth === month && gainYear === yearSelected) {
              try {
                amouthEntry += Number(gain.amount);
              } catch {
                throw new Error('AmountEntry is ivalid');
              }
            }
          });
          let amouthOutput = 0;
          expenses.forEach((value) => {
            const date = new Date(value.date);
            const expensesMonth = date.getMonth();
            const expensesYear = date.getFullYear();

            if (expensesMonth === month && expensesYear === yearSelected) {
              try {
                amouthOutput += Number(value.amount);
              } catch {
                throw new Error('amouthOutput is ivalid');
              }
            }
          });
          return {
            monthNumber: month,
            month: mouths[month].label.substr(0, 3),
            amouthEntry,
            amouthOutput,
          };
        })
        .filter((item) => {
          const currentMonth = new Date().getMonth();
          const currentYear = new Date().getFullYear();
          return (
            (yearSelected === currentYear &&
              item.monthNumber <= currentMonth) ||
            yearSelected < currentYear
          );
        }),
    [yearSelected]
  );

  return (
    <Container>
      <ContentHeader title={'Dashboard'} lineColor={'#e44c4e'}>
        <SelectInput
          options={mouths}
          onChange={(event) => setMonthSelected(Number(event.target.value))}
          defaultValue={monthSelected}
        />
        <SelectInput
          options={years}
          onChange={(event) => setYearSelected(Number(event.target.value))}
          defaultValue={yearSelected}
        />
      </ContentHeader>
      <Content>
        <WalletBox
          title={'Saldo'}
          amount={totalBalance}
          footerlabel={'Atualização com base nas etradas e saídas'}
          color={'#4e41f0'}
          icon={'dolar'}
        />
        <WalletBox
          title={'Entradas'}
          amount={totalGains}
          footerlabel={'Atualização com base nas etradas e saídas'}
          color={'#f7931B'}
          icon={'arrowUp'}
        />
        <WalletBox
          title={'Saída'}
          amount={totalExpansive}
          footerlabel={'Atualização com base nas etradas e saídas'}
          color={'#e44c4e'}
          icon={'arrowDown'}
        />
        <MensageBox
          title={mensageBalance.title}
          description={mensageBalance.description}
          footerText={mensageBalance.footerText}
          icon={mensageBalance.icon}
        />
        <PieChartBox data={realationExpensesVersusGains} />
        <HistoryBox
          data={historyData}
          lineColorAmountEntry="#f7931B"
          lineColorAmountOutput="#e44c4e"
        />
      </Content>
    </Container>
  );
};
export default Dashboard;
