import React, { useMemo, useState, useEffect } from 'react';
import { v4 as uuidv4 } from 'uuid';
import ContentHeader from '../../components/ContentHeader';
import SelectInput from '../../components/SelectInput';
import HistoryFinanceCard from '../../components/HistoryFinanceCard';
import { Container, Content, WrapperFilters } from './styled';
import gains from '../../utils/sampleData/gains';
import expenses from '../../utils/sampleData/expenses';
import formatCurrency from '../../utils/formatCurrency';
import formatDate from '../../utils/formatData';

interface IRouteParams {
  match: {
    params: {
      type: string;
    };
  };
}
interface IData {
  id: string;
  description: string;
  amountFormatted: string;
  tagColor: string;
  frequency: string;
  dateFormatted: string;
}

const List: React.FC<IRouteParams> = ({ match }) => {
  const [data, setData] = useState<IData[]>([]);
  const [monthSelected, setMonthSelected] = useState<number>(
    new Date().getMonth() + 1
  );
  const [yearSelected, setYearSelected] = useState<number>(
    new Date().getFullYear()
  );
  const [selectedFrequency, setSelectedFrequency] = useState<string[]>([
    'recorrente',
    'eventual',
  ]);

  const { type } = match.params;

  const titleMemo = useMemo(
    () =>
      type === 'entry-balance'
        ? {
            title: 'Entrada',
            lineColor: '#F7931b',
          }
        : {
            title: 'Saída',
            lineColor: '#E44c4e',
          },
    [type]
  );
  const listData = useMemo(
    () => (type === 'entry-balance' ? gains : expenses),
    [type]
  );

  const mouths = [
    { value: 1, label: 'Janeiro' },
    { value: 2, label: 'Fevereiro' },
    { value: 3, label: 'Março' },
    { value: 4, label: 'Abril' },
    { value: 5, label: 'Maio' },
    { value: 6, label: 'Junho' },
    { value: 7, label: 'Julho' },
    { value: 8, label: 'Agosto' },
    { value: 9, label: 'Setembro' },
    { value: 10, label: 'Outubro' },
    { value: 11, label: 'Novembro' },
    { value: 12, label: 'Dezembro' },
  ];

  const years = useMemo(() => {
    const uniqueYears: number[] = [];
    const currentYear = new Date().getFullYear();
    uniqueYears.push(currentYear);

    listData.forEach((item) => {
      const date = new Date(item.date);
      const year = date.getFullYear();

      if (!uniqueYears.includes(year)) {
        uniqueYears.push(year);
      }
    });

    return uniqueYears.map((year) => ({
      value: year,
      label: year,
    }));
  }, [listData]);

  useEffect(() => {
    const filteredData = listData.filter((item) => {
      const date = new Date(item.date);
      const mouth = date.getMonth() + 1;
      const year = date.getFullYear();

      return (
        mouth === monthSelected &&
        year === yearSelected &&
        selectedFrequency.includes(item.frequency)
      );
    });

    const responseData = filteredData.map((item) => ({
      id: uuidv4(),
      description: item.description,
      amountFormatted: formatCurrency(Number(item.amount)),
      tagColor: item.frequency === 'recorrente' ? '#4e41f0' : 'red',
      frequency: item.frequency,
      dateFormatted: formatDate(item.date),
    }));
    setData(responseData);
  }, [listData, monthSelected, yearSelected, selectedFrequency]);

  // logic of recurrent and eventual buttons
  const handleFrequencyClick = (frequency: string) => {
    const alreadySelected = selectedFrequency.findIndex(
      (item) => item === frequency
    );
    if (alreadySelected >= 0) {
      const filtered = selectedFrequency.filter((item) => item !== frequency);
      setSelectedFrequency(filtered);
    } else {
      setSelectedFrequency((prev) => [...prev, frequency]);
    }
  };

  return (
    <Container>
      <ContentHeader title={titleMemo.title} lineColor={titleMemo.lineColor}>
        <SelectInput
          options={mouths}
          onChange={(event) => setMonthSelected(Number(event.target.value))}
          defaultValue={monthSelected}
        />
        <SelectInput
          options={years}
          onChange={(event) => setYearSelected(Number(event.target.value))}
          defaultValue={yearSelected}
        />
      </ContentHeader>

      <WrapperFilters>
        <button
          type="button"
          onClick={() => handleFrequencyClick('recorrente')}
          className={`tag-filter tag-filter-recurrent ${
            selectedFrequency.includes('recorrente') && 'tag-actived'
          }`}
        >
          Recorrentes
        </button>
        <button
          type="button"
          onClick={() => handleFrequencyClick('eventual')}
          className={`tag-filter tag-filter-eventual ${
            selectedFrequency.includes('eventual') && 'tag-actived'
          }`}
        >
          Eventuais
        </button>
      </WrapperFilters>

      <Content>
        {data.map((item) => (
          <HistoryFinanceCard
            key={item.id}
            cardColor="#313862"
            tagColor={item.tagColor}
            title={item.description}
            subtitle={item.dateFormatted}
            amount={item.amountFormatted}
          />
        ))}
      </Content>
    </Container>
  );
};

export default List;
