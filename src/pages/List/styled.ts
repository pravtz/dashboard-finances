import styled from 'styled-components';

export const Container = styled.div``;
export const WrapperFilters = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  margin-bottom: 30px;
  .tag-filter {
    font-size: 18px;
    font-weight: 500;
    background: none;
    color: ${(props) => props.theme.colors.white};
    margin: 0 10px;
    opacity: 0.3;

    transition: opacity 0.2s;

    &:hover {
      opacity: 0.7;
    }
  }
  .tag-filter-recurrent::after {
    content: '';
    width: 55px;
    display: block;
    margin: 0 auto;
    border-bottom: 10px solid ${(props) => props.theme.colors.success};
  }
  .tag-filter-eventual::after {
    content: '';
    width: 55px;
    display: block;
    margin: 0 auto;
    border-bottom: 10px solid ${(props) => props.theme.colors.warning};
  }
  .tag-actived {
    opacity: 1;
  }
`;
export const Content = styled.div``;
